
#!/usr/bin/python3.5

import sys
import itertools

def main():

    In_pattern = sys.argv[1]
    
    XList = {}
    ct_x = 0
     
    for idx in range(len(In_pattern)):
        if (In_pattern[idx] == 'X'):
            XList[idx] = ct_x
            ct_x = ct_x + 1

    lst = [list(i) for i in itertools.product([0,1],repeat = ct_x)]
    
    for idx1 in range(len(lst)):
        outList = []
        for idx2 in range (len(In_pattern)):
            tmp = XList.get(idx2,"None")
            if (tmp is "None"):
                outList.append(In_pattern[idx2])
            else:
                outList.append(lst[idx1][XList.get(idx2)])

        outStr = ''.join(str(i) for i in outList)
        print (outStr)

if __name__ == "__main__":
    # execute only if run as a script
    main()